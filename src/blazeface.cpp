#include "execute.h"

#include <thread>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <memory>
#include <unistd.h>

#include <tflite_blazeface.h>

#include <opencv2/opencv.hpp>
#include <vector>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>

// for my_kbhit()
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>

blazeface_result_t face_ret;
int receivedFlag = 0;
cv::Mat img, resized_img;
std::vector<unsigned char> converted_img_for_gl;
std::vector<unsigned char> resized_img_for_gl;
const int size_tensor_dim1 = 128;
char key = 'd';
int input_img_width = 0;
int input_img_height = 0;

int kbhit(void)
{
    struct termios oldt, newt;
    int ch;
    int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    ch = getchar();

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    fcntl(STDIN_FILENO, F_SETFL, oldf);

    if (ch != EOF) {
        ungetc(ch, stdin);
        return 1;
    }

    return 0;
}

void set_image_to_gl_array(std::vector<unsigned char> &out_img, cv::Mat &cv_img,
						   const int num_rows, const int num_cols, const int num_rgba)
{
	// printf("%d %d\n", num_rows, num_cols);
	for (int row = 0; row < num_rows; row++)
		for (int col = 0; col < num_cols; col++)
		{
			const unsigned int curr_id_without_rgba = row * num_cols * num_rgba + col * num_rgba;
			auto r = cv_img.at<cv::Vec3b>(row, col)[2]; //Red
			auto g = cv_img.at<cv::Vec3b>(row, col)[1]; //Red
			auto b = cv_img.at<cv::Vec3b>(row, col)[0]; //Red
			out_img[curr_id_without_rgba + 0] = r;
			out_img[curr_id_without_rgba + 1] = g;
			out_img[curr_id_without_rgba + 2] = b;
			out_img[curr_id_without_rgba + 3] = 255; //Alpha
			// printf("%d %d %d %d\n", r, g, b, out_img[curr_id_without_rgba + 3]);
		}
}

void imageCb(const sensor_msgs::ImageConstPtr &msg)
{
	cv_bridge::CvImageConstPtr cv_ptr;
	try
	{
		receivedFlag = 1;
		cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
		// cvImg = cv_ptr->image;
		img = cv_ptr->image;

		cv::resize(img, resized_img, cv::Size(size_tensor_dim1, size_tensor_dim1)); // TODO: 128 -> variable
		const int num_rows = img.rows;
		const int num_cols = img.cols;
		const int num_rgba = 4;

		const bool need_init_width = (input_img_width != num_cols);
		const bool need_init_height = (input_img_height != num_rows);
		if (need_init_width && need_init_height)
		{
			auto init_img = cv::Mat::zeros(num_rows, num_cols, CV_8UC4);
			cv::imwrite("./base.png", init_img);
		}

		input_img_width = num_cols;
		input_img_height = num_rows;

		if (converted_img_for_gl.size() == 0)
		{
			converted_img_for_gl = std::vector<unsigned char>(num_rows * num_cols * num_rgba, 255);
			resized_img_for_gl = std::vector<unsigned char>(size_tensor_dim1 * size_tensor_dim1 * num_rgba, 255);
		}
		set_image_to_gl_array(converted_img_for_gl, img, num_rows, num_cols, num_rgba);
		set_image_to_gl_array(resized_img_for_gl, resized_img, size_tensor_dim1, size_tensor_dim1, num_rgba);
		set_image_array(&converted_img_for_gl[0], num_rows, num_cols, 1);
		set_image_array(&resized_img_for_gl[0], num_rows, num_cols, 0); // TODO: 第二第三引数クソなので直す

		receivedFlag = 0;
	}
	catch (cv_bridge::Exception &e)
	{
		printf("cv_bridge exception: %s", e.what());
		return;
	}
}

void execute_keyinput()
{
	while(1)
	{
		if (kbhit())
		{
			int curr_key = getchar();
			key = curr_key;	
			if (key == 'q')
				break;
		}
		usleep(50000);
	}
	std::terminate();
}

void execute_blazeface(int argc, char *argv[])
{
	usleep(500000);
	execute(argc, argv, &face_ret, &receivedFlag, &key);
}

void execute_ros(int argc, char *argv[])
{
	ros::init(argc, argv, "gl2blazeface");
	ros::NodeHandle n;

	ros::Publisher chatter_pub = n.advertise<std_msgs::String>("blazeface", 1000);
        ros::Rate loop_rate(10);

	int count = 0;
	printf("execute execute_ros().\n");
	
	image_transport::ImageTransport it(n);
	image_transport::Subscriber image_sub = it.subscribe("/camera/color/image_raw", 30, &imageCb);

	while (ros::ok())
	{
		// cv::imshow("sample", img);
		// cv::waitKey(0);
		// cv::destroyAllWindows();

		const int num_rows = img.rows;

		std_msgs::String msg;
		std::stringstream ss;
		for (int f_id = 0; f_id < face_ret.num; f_id++)
		{
			auto topleft = face_ret.faces[f_id].topleft;
			auto btmright = face_ret.faces[f_id].btmright;
			ss << "face id[" << f_id << "], " << "score:" << face_ret.faces[f_id].score
				<< ", topleft_x:" << topleft.x
				<< ", topleft_y:" << topleft.y
				<< ", btmright_x:" << btmright.x
				<< ", btmright_y:" << btmright.y << ";" ;
		}
		msg.data = ss.str();
		// ROS_INFO("%s", msg.data.c_str());
		chatter_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
		++count;
	}
}

int main(int argc, char *argv[])
{
	std::thread th_keyin(execute_keyinput);
	std::thread th_ros(execute_ros, argc, argv);
	std::thread th_bf(execute_blazeface, argc, argv);

	th_keyin.join();
	th_ros.join();
	th_bf.join();

	return 0;
}
