/* ------------------------------------------------ *
 * The MIT License (MIT)
 * Copyright (c) 2019 terryky1220@gmail.com
 * ------------------------------------------------ */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <float.h>
#include <GLES2/gl2.h>
#include "util_egl.h"
#include "util_debugstr.h"
#include "util_pmeter.h"
#include "util_texture.h"
#include "util_render2d.h"
#include "tflite_blazeface.h"
#include "camera_capture.h"
#include "video_decode.h"
#include "render_imgui.h"

#include "execute.h"

#define UNUSED(x) (void)(x)

unsigned char *buf_external_image = NULL;
unsigned char *buf_external_resized_image = NULL;
int external_img_cap_h = 0;
int external_img_cap_w = 0;

void
set_image_array(unsigned char *external_image, const int cap_h, const int cap_w, const int is_src_image)
{
	external_img_cap_h = cap_h;
	external_img_cap_w = cap_w;

    if (is_src_image)
        buf_external_image = external_image;
    else
        buf_external_resized_image = external_image;
}

static void
update_ros_texture(texture_2d_t *captex)
{
    int   video_w, video_h;
    uint32_t video_fmt;
    void *ros_img_buf;

    ros_img_buf = buf_external_image;

    if (ros_img_buf == NULL)
        return;

    if (ros_img_buf) 
    {
	// printf("update img!!!!!\n");
        int texw = external_img_cap_w;
        int texh = external_img_cap_h;
        int texfmt = GL_RGBA;

        glBindTexture (GL_TEXTURE_2D, captex->texid);
        glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, texw, texh, texfmt, GL_UNSIGNED_BYTE, ros_img_buf);
    }
}

/* resize image to DNN network input size and convert to fp32. */
void
feed_blazeface_image(texture_2d_t *srctex, int win_w, int win_h)
{
    int x, y, w, h;
    float *buf_fp32 = (float *)get_blazeface_input_buf (&w, &h);
    unsigned char *buf_ui8 = NULL;
    static unsigned char *pui8 = NULL;

    if (pui8 == NULL)
        pui8 = (unsigned char *)malloc(w * h * 4);

    buf_ui8 = pui8;

    draw_2d_texture_ex (srctex, 0, win_h - h, w, h, 1);
    glPixelStorei (GL_PACK_ALIGNMENT, 4);
    glReadPixels (0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buf_ui8);

    if (buf_external_image != NULL)
    {
        buf_ui8 = buf_external_resized_image;
    }

    float mean = 128.0f;
    float std  = 128.0f;
    for (y = 0; y < h; y ++)
    {
        for (x = 0; x < w; x ++)
        {
            int r = *buf_ui8 ++;
            int g = *buf_ui8 ++;
            int b = *buf_ui8 ++;
            int a = *buf_ui8 ++;
            // buf_ui8 ++;          /* skip alpha */
            *buf_fp32 ++ = (float)(r - mean) / std;
            *buf_fp32 ++ = (float)(g - mean) / std;
            *buf_fp32 ++ = (float)(b - mean) / std;
        }
    }
    return;
}


static void
render_detect_region (int ofstx, int ofsty, int texw, int texh, blazeface_result_t *detection, imgui_data_t *imgui_data)
{
    float col_white[] = {1.0f, 1.0f, 1.0f, 1.0f};
    float *col_frame = imgui_data->frame_color;

    for (int i = 0; i < detection->num; i ++)
    {
        face_t *face = &(detection->faces[i]);
        float x1 = face->topleft.x  * texw + ofstx;
        float y1 = face->topleft.y  * texh + ofsty;
        float x2 = face->btmright.x * texw + ofstx;
        float y2 = face->btmright.y * texh + ofsty;
        float score = face->score;

        /* rectangle region */
        draw_2d_rect (x1, y1, x2-x1, y2-y1, col_frame, 2.0f);

        /* class name */
        char buf[512];
        sprintf (buf, "%d", (int)(score * 100));
        draw_dbgstr_ex (buf, x1, y1, 1.0f, col_white, col_frame);

        /* key points */
        for (int j = 0; j < kFaceKeyNum; j ++)
        {
            float x = face->keys[j].x * texw + ofstx;
            float y = face->keys[j].y * texh + ofsty;

            int r = 4;
            draw_2d_fillrect (x - (r/2), y - (r/2), r, r, col_frame);
        }
    }
}


/* Adjust the texture size to fit the window size
 *
 *                      Portrait
 *     Landscape        +------+
 *     +-+------+-+     +------+
 *     | |      | |     |      |
 *     | |      | |     |      |
 *     +-+------+-+     +------+
 *                      +------+
 */
static void
adjust_texture (int win_w, int win_h, int texw, int texh, 
                int *dx, int *dy, int *dw, int *dh)
{
    float win_aspect = (float)win_w / (float)win_h;
    float tex_aspect = (float)texw  / (float)texh;
    float scale;
    float scaled_w, scaled_h;
    float offset_x, offset_y;

    if (win_aspect > tex_aspect)
    {
        scale = (float)win_h / (float)texh;
        scaled_w = scale * texw;
        scaled_h = scale * texh;
        offset_x = (win_w - scaled_w) * 0.5f;
        offset_y = 0;
    }
    else
    {
        scale = (float)win_w / (float)texw;
        scaled_w = scale * texw;
        scaled_h = scale * texh;
        offset_x = 0;
        offset_y = (win_h - scaled_h) * 0.5f;
    }

    *dx = (int)offset_x;
    *dy = (int)offset_y;
    *dw = (int)scaled_w;
    *dh = (int)scaled_h;
}

void
setup_imgui (int win_w, int win_h, imgui_data_t *imgui_data)
{

    imgui_data->frame_color[0] = 1.0f;
    imgui_data->frame_color[1] = 0.0f;
    imgui_data->frame_color[2] = 0.0f;
    imgui_data->frame_color[3] = 1.0f;
}

/*--------------------------------------------------------------------------- *
 *      M A I N    F U N C T I O N
 *--------------------------------------------------------------------------- */
const float
// execute(int argc, char *argv[], float *out_score)
execute(int argc, char *argv[], blazeface_result_t *out_face_ret, int *receivedFlag, char *key)
{
    char input_name_default[] = "";
    char *input_name = NULL;
    int count;
    int win_w = 960;
    int win_h = 540;
    int texid;
    int texw, texh, draw_x, draw_y, draw_w, draw_h;
    texture_2d_t captex = {0};
    double ttime[10] = {0}, interval, invoke_ms;
    int use_quantized_tflite = 0;
    int enable_camera = 1;
    imgui_data_t imgui_data = {0};
    UNUSED (argc);
    UNUSED (*argv);

    {
        int c;
        const char *optstring = "qv:x";

        while ((c = getopt (argc, argv, optstring)) != -1)
        {
            switch (c)
            {
            case 'q':
                use_quantized_tflite = 1;
                break;
            case 'x':
                enable_camera = 0;
                break;
            }
        }

        while (optind < argc)
        {
            input_name = argv[optind];
            optind++;
        }
    }

    if (input_name == NULL)
        input_name = input_name_default;

    egl_init_with_platform_window_surface (2, 0, 0, 0, win_w, win_h);

    init_2d_renderer (win_w, win_h);
    init_pmeter (win_w, win_h, 500);
    init_dbgstr (win_w, win_h);
    init_tflite_blazeface (use_quantized_tflite, &imgui_data.blazeface_config);

    setup_imgui (win_w, win_h, &imgui_data);

#if defined (USE_GL_DELEGATE) || defined (USE_GPU_DELEGATEV2)
    /* we need to recover framebuffer because GPU Delegate changes the context */
    glBindFramebuffer (GL_FRAMEBUFFER, 0);
    // glViewport (0, 0, win_w, win_h);
#endif

    {
        load_png_texture("base.png", &texid, &texw, &texh);
        captex.texid = texid;
        captex.width = texw;
        captex.height = texh;
        captex.format = pixfmt_fourcc('R', 'G', 'B', 'A');
    }

    adjust_texture (win_w, win_h, texw, texh, &draw_x, &draw_y, &draw_w, &draw_h);

    glClearColor (0.f, 0.f, 0.f, 1.0f);

    // *out_score = (float)rand() / RAND_MAX;
#if 1
    // float ret_score = 0.0;
    for (count = 0; ; count ++)
    {
		// printf("key: %c\n", *key);
        // if (*key == 'q')
        //     exit();
        // printf("count:%d\n", count);

        // if (*receivedFlag == 1)
        //     continue;
        blazeface_result_t face_ret = {0};
        char strbuf[512];

        PMETER_RESET_LAP ();
        PMETER_SET_LAP ();

        ttime[1] = pmeter_get_time_ms ();
        interval = (count > 0) ? ttime[1] - ttime[0] : 0;
        ttime[0] = ttime[1];

        glClear (GL_COLOR_BUFFER_BIT);
	// printf("execute update_ros_texture()!!!!!\n");
        update_ros_texture(&captex);

        /* invoke pose estimation using TensorflowLite */
        feed_blazeface_image (&captex, win_w, win_h);

        ttime[2] = pmeter_get_time_ms ();

		// printf("before invoke_blazeface\n");
        invoke_blazeface(&face_ret, &imgui_data.blazeface_config);
		// printf("after invoke_blazeface\n");

        ttime[3] = pmeter_get_time_ms ();
        invoke_ms = ttime[3] - ttime[2];

        glClear (GL_COLOR_BUFFER_BIT);

        /* visualize the object detection results. */
        draw_2d_texture_ex (&captex, draw_x, draw_y, draw_w, draw_h, 0);
        render_detect_region (draw_x, draw_y, draw_w, draw_h, &face_ret, &imgui_data);

        draw_pmeter (0, 40);

        *out_face_ret = face_ret;

        sprintf (strbuf, "Interval:%5.1f [ms]\nTFLite  :%5.1f [ms]", interval, invoke_ms);
        draw_dbgstr (strbuf, 10, 10);

        egl_swap();
    }
#endif
    return 0.0;
}

