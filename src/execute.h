/* ------------------------------------------------ *
 * The MIT License (MIT)
 * Copyright (c) 2019 terryky1220@gmail.com
 * ------------------------------------------------ */
#ifndef EXECUTE_H_
#define EXECUTE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "tflite_blazeface.h"

// extern const float execute(int argc, char *argv[], float *out_score);
extern void set_image_array(unsigned char *external_image, const int cap_h, const int cap_w, const int is_src_image);
extern const float execute(int argc, char *argv[], blazeface_result_t *out_face_ret, int *receivedFlag, char *key);

#ifdef __cplusplus
}
#endif

#endif
