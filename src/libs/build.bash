#!/bin/bash

export LD_LIBRARY_PATH=~/lib:$LD_LIBRARY_PATH

sudo rm ./build/ -r
mkdir ./build
cd ./build
rm ./*.a

SCRIPT_DIR=$(cd $(dirname $0); pwd)
# PACKAGE_ROOT_DIR="/home/ksato/work/tflite_gles_app"
PACKAGE_ROOT_DIR="__PLEASE_REPLACE_PATH__"

cc_file_list=(${PACKAGE_ROOT_DIR}/common/util_texture.c ${PACKAGE_ROOT_DIR}/common/assertgl.c ${PACKAGE_ROOT_DIR}/common/assertegl.c ${PACKAGE_ROOT_DIR}/common/util_egl.c ${PACKAGE_ROOT_DIR}/common/util_shader.c ${PACKAGE_ROOT_DIR}/common/util_matrix.c ${PACKAGE_ROOT_DIR}/common/util_render2d.c ${PACKAGE_ROOT_DIR}/common/util_debugstr.c ${PACKAGE_ROOT_DIR}/common/util_pmeter.c ${PACKAGE_ROOT_DIR}/common/winsys/winsys_x11.c ${PACKAGE_ROOT_DIR}/gl2blazeface/camera_capture.c ${PACKAGE_ROOT_DIR}/common/util_v4l2.c ${PACKAGE_ROOT_DIR}/common/util_drm.c)

for elem in ${cc_file_list[@]}; do
	sudo rm CMake* -r
	f_name=$(basename ${elem})
	echo ${f_name}
	echo ${elem}
	cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DUSE_C_COMPILER=true
	# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTARGET_ENV=jetson_nano -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=true
	# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=true
	# make -j12
	make -j12 TARGET_ENV=jetson_nano TFLITE_DELEGATE=GPU_DELEGATEV2
done

gpp_file_list=(${PACKAGE_ROOT_DIR}/gl2blazeface/tflite_blazeface.cpp ${PACKAGE_ROOT_DIR}/common/util_tflite.cpp )

for elem in ${gpp_file_list[@]}; do
	sudo rm CMake* -r
	f_name=$(basename ${elem})
	echo ${f_name}
	echo ${elem}
	cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DUSE_C_COMPILER=false
	# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTARGET_ENV=jetson_nano -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=false
	# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=false
	# make -j12 
	make -j12 TARGET_ENV=jetson_nano TFLITE_DELEGATE=GPU_DELEGATEV2
done

sudo rm CMake* -r
f_name="execute.c"
elem="../execute.c"
cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DUSE_C_COMPILER=true
# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTARGET_ENV=jetson_nano -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=true
# cmake .. -DMY_BUILD_FILE_PATH=${elem} -DMY_BUILD_FILE=${f_name} -DTFLITE_DELEGATE=GPU_DELEGATEV2 -DUSE_C_COMPILER=true
# make -j12
make -j12 TARGET_ENV=jetson_nano TFLITE_DELEGATE=GPU_DELEGATEV2
