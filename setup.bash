#!/bin/bash
echo "-----------------------------------------"

# Set "tflite_gles_app_path"
echo -n "  Please tflite_gles_app path [~/work/tflite_gles_app] : "
read input

if [ -z $input ]; then
    tflite_gles_app_path=$HOME"/work/tflite_gles_app"
else
    tflite_gles_app_path=$input
fi

if [ ! -d $tflite_gles_app_path ]; then
    echo "    Could Not Find This directory :" $tflite_gles_app_path
    echo "/////////////////////////////////////////"
    exit;
fi

echo "-----------------------------------------"

for_sed_path=${tflite_gles_app_path////\\/}

sed -i -e "s/__PLEASE_REPLACE_PATH__/$for_sed_path/" ./CMakeLists.txt ./src/libs/CMakeLists.txt ./src/libs/build.bash

cp ${tflite_gles_app_path}/gl2blazeface/blazeface_model ./ -r

cd src/libs/ && bash build.bash

echo "-----------------------------------------"
