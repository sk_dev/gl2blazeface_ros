# Overview
This ros node can lightweight and well-performing face detection using [tflite_gles_app](https://github.com/terryky/tflite_gles_app).

# TODO
- [ ] #2
- [ ] #3
- [ ] #5

# Environment
- Ubuntu 18.04 LTS
- ROS melodic

# Prerequirement
Follow [these instructions](https://github.com/terryky/tflite_gles_app#2-how-to-build--run) to build tensorflow lite libs beforehand.

# Usage in other than catkin_ws

## Build 
```
~/path/to/your/gl2blazeface_ros$ bash setup.bash
~/path/to/your/gl2blazeface_ros$ mkdir build 
~/path/to/your/gl2blazeface_ros$ cd build 
~/path/to/your/gl2blazeface_ros/build$ cmake ..
~/path/to/your/gl2blazeface_ros/build$ make -j12
```

## Rebuild
When rebuild, clean your path to `tflite_gles_app` before execute `bash setup.bash`.
```
~/path/to/your/gl2blazeface_ros$ bash clean_lib_path.bash
```

## Run
- Terminal1
```
~$ roscore
```

- Terminal2
```
~$ rosrun uvc_camera uvc_camera_node /image_raw:=/camera/color/image_raw
```

- Terminal3
```
~/path/to/your/gl2blazeface_ros/build$ ./blazeface
```

## Terminate
On the terminal while executing `./blazeface`, press key `q`.
